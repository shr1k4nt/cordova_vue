const path = require("path");

module.exports = {
    indexPath: path.resolve(__dirname, '../www/index.html'),
    outputDir: path.resolve(__dirname, "../www"),
    publicPath: "",
    productionSourceMap: false,
    lintOnSave: false
}